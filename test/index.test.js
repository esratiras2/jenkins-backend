const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
chai.use(chaiHttp);
const expect = chai.expect

describe("First Test", function () {

    it("testing...", function () {
            chai.request('http://localhost:3000')
                .get('/')
                .end(function (err, res) {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                });
    });

});
